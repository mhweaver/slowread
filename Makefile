build/slowread:
	go build -o build/slowread .

install-bin:	build/slowread
	cp $< ${HOME}/bin

docker-build:
	docker build --pull --rm -f Dockerfile -t slowread .

install:	docker-build
	cp slowread ${HOME}/bin

clean:
	rm -rf build

.PHONY:	docker-build clean