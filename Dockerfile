FROM golang:1.14.2-alpine3.11 as build
WORKDIR /src
COPY . .
RUN apk add make \
  && make

FROM scratch
COPY --from=build /src/build/slowread slowread

CMD ["./slowread"]